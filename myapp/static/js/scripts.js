$(function() {
	// (Optional) Active an item if it has the class "is-active"	
	$(".accordion > .accordion-item.is-active").children(".accordion-panel").slideDown();
	
	$(".accordion > .accordion-item").click(function() {
		// Cancel the siblings
		$(this).siblings(".accordion-item").removeClass("is-active").children(".accordion-panel").slideUp();
		// Toggle the item
		$(this).toggleClass("is-active").children(".accordion-panel").slideToggle("ease-out");
	});
});


$(document).ready(function(){
    $('.change-button').click(function(){
        $('.toggle').toggleClass('active')
        $('body').toggleClass('night')
        $('.jumbotron').toggleClass('night')
        $('.row').toggleClass('night')

		if ($('.me').attr('src') === '/static/img/me.jpg'){
			$('.me').attr('src', '/static/img/night.jpg');

		} 
		else {
			$('.me').attr('src', '/static/img/me.jpg')
			
		}
    })
})
